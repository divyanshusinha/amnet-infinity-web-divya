import { Injectable } from '@angular/core';

//used to share dat abtw components
@Injectable()
export class DataService {

  lookups = [
												{"Name":"Alsssfreds Futterkiste","Description":"Berlin","Country":"Germany"},
												{"Name":"Ana Trujillo Emparedados y helados","Description":"México D.F.","Country":"Mexico"},
												{"Name":"Antonio Moreno Taquería","Description":"México D.F.","Country":"Mexico"},
												{"Name":"Around the Horn","Description":"London","Country":"UK"},
												{"Name":"B's Beverages","Description":"London","Country":"UK"},
												{"Name":"Berglunds snabbköp","Description":"Luleå","Country":"Sweden"},
												{"Name":"Blauer See Delikatessen","Description":"Mannheim","Country":"Germany"},
												{"Name":"Blondel père et fils","Description":"Strasbourg","Country":"France"},
												{"Name":"Bólido Comidas preparadas","Description":"Madrid","Country":"Spain"},
												{"Name":"Bon app'","Description":"Marseille","Country":"France"},
												{"Name":"Bottom-Dollar Marketse","Description":"Tsawassen","Country":"Canada"},
												{"Name":"Cactus Comidas para llevar","Description":"Buenos Aires","Country":"Argentina"},
												{"Name":"Centro comercial Moctezuma","Description":"México D.F.","Country":"Mexico"},
												{"Name":"Chop-suey Chinese","Description":"Bern","Country":"Switzerland"},
												{"Name":"Comércio Mineiro","Description":"São Paulo","Country":"Brazil"}
											 ];
	 

    constructor() { }

 

}
