import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Environment } from '../app-settings/env';
import { AuthHandler } from  '../app-settings/auth-util/auth-handler';
import { PermissionsHandler } from  '../app-settings/auth-util/permissions-handler';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['../globalStyle.css']
})
export class HeaderComponent implements OnInit {

 rolesAndPermissions: any[] = [];
 isCommercialViewAllowed=false;
 isSuperUser=false;
 constructor(public cookieService: CookieService, public router: Router, public authHandler: AuthHandler, public permissionsHandler: PermissionsHandler) {
     this.rolesAndPermissions=authHandler.getRolesAndPermissions();
     this.isCommercialViewAllowed=permissionsHandler.isCommercialViewAllowed();
     this.isSuperUser=this.permissionsHandler.isSuperUser();
     console.log("issuper:"+this.isSuperUser);
 }

 logoutApp(){
    this.authHandler.logoutApp();
  }
  
 viewLookups(){
    this.router.navigate(['/','lookupMaintenance/lookupInventory']);
 }

 ngOnInit(): void {}
   
}

// http://localhost:4200/#
// access_token=eyJhbGciOiJSUzI1NiJ9.eyJleHAiOjE1MTM0Nzc3MDQsInVzZXJfbmFtZSI6InN1YmhhbkBtZWRpYWlxZGlnaXRhbC5jb20iLCJzY29wZSI6WyJEQVRBU0VUX0NSRUFURSIsIkZFQVRVUkVfREVMRVRFIiwiUExBVEZPUk1fQ1JFQVRFIiwiVEVNUExBVEVfQ1JFQVRFIiwiV09SS0ZMT1dfQ1JFQVRFIiwiREFUQVNFVF9WSUVXIiwiSU5TSUdIVFNfQ1JFQVRFIiwiVEVNUExBVEVfREVMRVRFIiwiVEVNUExBVEVfVklFVyIsIkZFQVRVUkVfQ1JFQVRFIiwiREFUQVNFVF9ERUxFVEUiLCJXT1JLRkxPV19ERUxFVEUiLCJGRUFUVVJFX1ZJRVciLCJXT1JLRkxPV19WSUVXIiwiUExBVEZPUk1fREVMRVRFIiwiUExBVEZPUk1fVklFVyIsIldPUktGTE9XX01PTklUT1IiXSwiYXV0aG9yaXRpZXMiOlsiQU1BUF9ERVYiLCJBSVFYX0NMSUVOVCIsIkRFViIsIkJJX1NUQU5EQVJEX1VTRVIiLCJBSVFYX0NMSUVOVF9BRE1JTiJdLCJhdWQiOlsiYW1hcCJdLCJqdGkiOiIzYjQ1ZTMyYi1iMzc4LTQxNjAtYjUyNC03N2EwMjI2M2RlZWEiLCJjbGllbnRfaWQiOiJhbWFwIn0.nngVNJKtYYi4Ad0sJ-9ZH5YVIehwWJ-Jg30MGmFYAISfeHAqWhibdowTX6ZJtImKCJZfhEG35sUQP9UT1s7T8e4uoIAA4vZleCXuJHphjpgc9sxjha-JxWiNkDwemiiMI9GB9A3OP6PvSDkwZ6fpPFvWCbOouamg94zIWoKZDSJsXXNyKIRL0ulg5QPQOfqXsTjGfLvqFxBcVZzneGZjGBjcVvSE2qKOrMZZ3txsnYIO379VjJ8fVjzmX31H-Awc-cIS11Ckmqxs02IItw9IawnXGZGpt17vw_zJ3B-YimL_lc_5o5KRYhJh40YwWCKKZmUC0lYfN48xu4GWjNjm3Q
// &token_type=bearer&expires_in=43199&scope=NA
// &jti=3b45e32b-b378-4160-b524-77a02263deea

//eyJhbGciOiJSUzI1NiJ9.eyJleHAiOjE1MTUwMDA1NjcsInVzZXJfbmFtZSI6ImFtYXAtZGV2Iiwic2NvcGUiOlsiREFUQVNFVF9DUkVBVEUiLCJGRUFUVVJFX0RFTEVURSIsIlBMQVRGT1JNX0NSRUFURSIsIlRFTVBMQVRFX0NSRUFURSIsIldPUktGTE9XX0NSRUFURSIsIkRBVEFTRVRfVklFVyIsIklOU0lHSFRTX0NSRUFURSIsIlRFTVBMQVRFX0RFTEVURSIsIlRFTVBMQVRFX1ZJRVciLCJGRUFUVVJFX0NSRUFURSIsIkRBVEFTRVRfREVMRVRFIiwiV09SS0ZMT1dfREVMRVRFIiwiRkVBVFVSRV9WSUVXIiwiV09SS0ZMT1dfVklFVyIsIlBMQVRGT1JNX0RFTEVURSIsIlBMQVRGT1JNX1ZJRVciLCJXT1JLRkxPV19NT05JVE9SIl0sImF1dGhvcml0aWVzIjpbIkFNQVBfREVWIiwiSU5GSU5JVFlfREVWIl0sImF1ZCI6WyJhbWFwIl0sImp0aSI6IjczNWY1NDJmLTViNmMtNGU3NC1hMTY3LTZhNDZkYWQ1Y2YwYyIsImNsaWVudF9pZCI6ImluZmluaXR5In0.FdmiD8vT5yDTUlZJIUV-HlrCtvc3u4q3zxQ7yokwhSlKSqFfmmbriu056JcLwvKIMrEQiaw03RamsUUd-9ksj5YMyDZJf4_nNMTsnkDwv9YdyBCIp5zZObhVqMBYtewKq-v2iBhAgT_sCx2e2JFwqLY7-F19oTFHrsoJV_37njtabtkdd1_srC2K6EgtYyVnvD-FpMCFnRMI1lWBtL5dVEjdD8wfVsnrmZZ7mjA8dGlsaZmJ4iVZT55imry4YU8JQ5vkRml2WB4qwnztEabgQcGjsayll9E3dl6B0PDeKZiFhw9ZQPMKQd7382yc3AEWXqVlvwo0Utc-cUNna2uH9Q