import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['../globalStyle.css']
})
export class HomeComponent implements OnInit {
  constructor(private router: Router) {
  }
  ngOnInit() {
  }
}

// // http://localhost:4200/#
// // access_token=eyJhbGciOiJSUzI1NiJ9.eyJleHAiOjE1MTM0Nzc3MDQsInVzZXJfbmFtZSI6InN1YmhhbkBtZWRpYWlxZGlnaXRhbC5jb20iLCJzY29wZSI6WyJEQVRBU0VUX0NSRUFURSIsIkZFQVRVUkVfREVMRVRFIiwiUExBVEZPUk1fQ1JFQVRFIiwiVEVNUExBVEVfQ1JFQVRFIiwiV09SS0ZMT1dfQ1JFQVRFIiwiREFUQVNFVF9WSUVXIiwiSU5TSUdIVFNfQ1JFQVRFIiwiVEVNUExBVEVfREVMRVRFIiwiVEVNUExBVEVfVklFVyIsIkZFQVRVUkVfQ1JFQVRFIiwiREFUQVNFVF9ERUxFVEUiLCJXT1JLRkxPV19ERUxFVEUiLCJGRUFUVVJFX1ZJRVciLCJXT1JLRkxPV19WSUVXIiwiUExBVEZPUk1fREVMRVRFIiwiUExBVEZPUk1fVklFVyIsIldPUktGTE9XX01PTklUT1IiXSwiYXV0aG9yaXRpZXMiOlsiQU1BUF9ERVYiLCJBSVFYX0NMSUVOVCIsIkRFViIsIkJJX1NUQU5EQVJEX1VTRVIiLCJBSVFYX0NMSUVOVF9BRE1JTiJdLCJhdWQiOlsiYW1hcCJdLCJqdGkiOiIzYjQ1ZTMyYi1iMzc4LTQxNjAtYjUyNC03N2EwMjI2M2RlZWEiLCJjbGllbnRfaWQiOiJhbWFwIn0.nngVNJKtYYi4Ad0sJ-9ZH5YVIehwWJ-Jg30MGmFYAISfeHAqWhibdowTX6ZJtImKCJZfhEG35sUQP9UT1s7T8e4uoIAA4vZleCXuJHphjpgc9sxjha-JxWiNkDwemiiMI9GB9A3OP6PvSDkwZ6fpPFvWCbOouamg94zIWoKZDSJsXXNyKIRL0ulg5QPQOfqXsTjGfLvqFxBcVZzneGZjGBjcVvSE2qKOrMZZ3txsnYIO379VjJ8fVjzmX31H-Awc-cIS11Ckmqxs02IItw9IawnXGZGpt17vw_zJ3B-YimL_lc_5o5KRYhJh40YwWCKKZmUC0lYfN48xu4GWjNjm3Q
// // &token_type=bearer&expires_in=43199&scope=NA
// // &jti=3b45e32b-b378-4160-b524-77a02263deea