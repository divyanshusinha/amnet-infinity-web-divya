import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Environment } from '../../app-settings/env';
import { AuthHandler } from './auth-handler'
import { Component, OnInit } from '@angular/core';
@Injectable()
export class PermissionsHandler implements OnInit {

  private static SUPPLY_PARTNER_CONTROLS="SUPPLY_PARTNER_CONTROLS";
  private static INFINTY_SPC_COMMERCIAL_USER="false";
  INFINTY_SPC_TRADER_USER="false";
  INFINTY_SPC_COMMERCIAL_ADMIN_USER : boolean=false;
  INFINTY_SPC_COMMERCIAL_VIEW_USER : boolean=false;
  INFINITY_SUPER_USER : boolean=false;


  private MANAGE_SPC_COMMERCIAL_INPUT_VIEW_PERMISSION="MANAGE_SPC_COMMERCIAL_INPUT_VIEW";
  private MANAGE_SPC_COMMERCIAL_INPUT_CREATE_PERMISSION="MANAGE_SPC_COMMERCIAL_INPUT_CREATE";
  private MANAGE_SPC_COMMERCIAL_INPUT_DELETE_PERMISSION="MANAGE_SPC_COMMERCIAL_INPUT_DELETE";

   OPTIMIZE_DELETE_PERMISSION="OPTIMIZE_DELETE";
   ADMIN_DELETE_PERMISSION="ADMIN_DELETE";
   MANAGE_DELETE_PERMISSION="MANAGE_DELETE";
   DISCOVER_DELETE_PERMISSION="DISCOVER_DELETE";

  rolesAndPermissions = [];
  AUTH_URL_TOKEN_COOKIE='token';
  cookieValue = '';
  permissionsList='';
  

  constructor(public cookieService: CookieService,public authHandler: AuthHandler) {
       // this.rolesAndPermissions=JSON.parse(this.authHandler.getRolesAndPermissions());
       let allRoles=JSON.stringify(this.authHandler.getRolesAndPermissions());
       console.log("allRoles"+JSON.parse(allRoles).scope);
         console.log("RolesAndPermissions"+JSON.stringify(this.authHandler.getRolesAndPermissions()));
         this.rolesAndPermissions=JSON.parse(allRoles).scope;
        console.log("RolesAndPermissions"+JSON.stringify(this.rolesAndPermissions));
        this.setSupplyPartnerControlAccess();
        this.setSuperUser();
  }
  
  ngOnInit(): void{}
  
  private setSupplyPartnerControlAccess(){
     //var count = Object.keys(this.rolesAndPermissions).length;
    // console.log(count);
    //if(count != 0)
    {
    if(this.rolesAndPermissions.indexOf(this.MANAGE_SPC_COMMERCIAL_INPUT_VIEW_PERMISSION) !== -1) {
      this.INFINTY_SPC_COMMERCIAL_VIEW_USER=true;
      console.log("setting INFINTY_SPC_COMMERCIAL_VIEW_USER");
    } else if(this.rolesAndPermissions.indexOf(this.MANAGE_SPC_COMMERCIAL_INPUT_CREATE_PERMISSION) !== -1) {
      this.INFINTY_SPC_COMMERCIAL_ADMIN_USER=true;
    } else{
      console.log("did not find role");
    }
   }

  }

  private setSuperUser(){
    //console.log("this.rolesAndPermissions.scope"+this.rolesAndPermissions.toString());
     if((this.rolesAndPermissions.indexOf("OPTIMIZE_DELETE") !== -1) 
      && (this.rolesAndPermissions.indexOf(this.ADMIN_DELETE_PERMISSION) !== -1)
      && (this.rolesAndPermissions.indexOf(this.DISCOVER_DELETE_PERMISSION) !== -1) 
      && (this.rolesAndPermissions.indexOf(this.MANAGE_DELETE_PERMISSION) !== -1)){
          this.INFINITY_SUPER_USER=true;
        console.log("Is a Super User");

       }else{
      console.log("Not a Super User");
    }
  } 
  
 public isCommercialViewAllowed(){
  return this.INFINTY_SPC_COMMERCIAL_VIEW_USER;
 }

 public isCommercialAdmin(){
  return this.INFINTY_SPC_COMMERCIAL_ADMIN_USER;
 }

 public isSuperUser(){
  return this.INFINITY_SUPER_USER;
 }

}