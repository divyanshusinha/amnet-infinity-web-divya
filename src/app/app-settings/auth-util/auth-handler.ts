import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Environment } from '../../app-settings/env';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
@Injectable()
export class AuthHandler implements OnInit {

  public static SUPPLY_PARTNER_CONTROLS="SUPPLY_PARTNER_CONTROLS";
  public static INFINTY_SPC_COMMERCIAL_USER="false";
  public static INFINTY_SPC_TRADER_USER="false";
  public static INFINTY_SPC_COMMERCIAL_ADMIN_USER="false";
  public static INFINTY_SPC_COMMERCIAL_VIEW_USER="false";
  AUTH_URL_TOKEN_COOKIE='token';
  cookieValue = '';
  permissionsList='';
  rolesAndPermissions=[];
  constructor(public cookieService: CookieService, public router: Router) {
        this.loginApp();
  }
  
  ngOnInit(): void{}
  

  public getRolesAndPermissions(){
    return this.rolesAndPermissions;
  }
  
  public loginApp(){
    var location = window.location;
    var hash = location.hash;
    if(hash == ''){
      location.href=Environment.AUTH_URL+Environment.CURRENT_HOST;
    }else{
      var accessToken = hash.split('&')[0].substring('#access_token='.length);
      document.cookie = this.AUTH_URL_TOKEN_COOKIE + "=" + accessToken; 
      var jwtData = accessToken.split('.')[1];
      let decodedJwtJsonData = window.atob(jwtData);
      this.rolesAndPermissions=JSON.parse(decodedJwtJsonData);
      this.router.navigate(['home']);
   } 
  } 

  public logoutApp(){
    window.location.href=Environment.AUTH_LOGOUT_URL;
    this.router.navigate(['']);
  }

  public setSupplyPartnerControlAccess(){
    
  }
}
  

