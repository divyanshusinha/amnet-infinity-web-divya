import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommercialKitComponent } from './commercial-kit.component';

describe('CommercialKitComponent', () => {
  let component: CommercialKitComponent;
  let fixture: ComponentFixture<CommercialKitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommercialKitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommercialKitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
